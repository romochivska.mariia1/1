﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Завдання 1

            List<string> elements = new List<string>
            {
                "Гiдроген",
                "Оксиген",
                "Гелiй",
                "Лiтiй",
                "Берилiй",
                "Неон",
                "Бор",
                "Карбон",
            };

            elements.Add("Натрiй");
            elements.Add("Магнiй");

            Console.WriteLine("Елементи у прямому порядку:");
            foreach (string element in elements)
            {
                Console.WriteLine(element);
            }

            Console.WriteLine("\nЕлементи у зворотному порядку:");

            List<string> reversedElements = new List<string>(elements);

            reversedElements.Reverse();
            foreach (element in reversedElements)
            {
                Console.WriteLine(element);
            }

            Console.WriteLine("\nКiлькiсть елементiв у колекцiї: " + elements.Count);


            elements.Clear();

            //Завдання 2

           
            Queue<int> queue = new Queue<int>();

            queue.Enqueue(2);
            queue.Enqueue(4);
            queue.Enqueue(-2);
            queue.Enqueue(8);
            queue.Enqueue(6);

            int sum = 0;
            int count = 0;

            while (queue.Count > 0)
            {
                int item = queue.Dequeue();
                if (item >= -3 && item <= 5)
                {
                    sum += item;
                    count++;
                }
            }

            if (count > 0)
            {
                double average = (double)sum / count;
                Console.WriteLine($"Середнє арифметичне значення елементiв в дiапазонi [-3; 5]: {average}");
            }
            else
            {
                Console.WriteLine("Елементiв, що задовольняють вимогам, в черзi немає");
            }

            Console.ReadLine();
        }
    }
}
